import { useState } from 'react';
import ProductList from './ProductList';

const Home = () => {

    const [products, setProducts] = useState([
        { title: "carrot", price: 0.50, quantity: 916, id: 1 },
        { title: "cucumber", price: 0.32, quantity: 520, id: 2 },
        { title: "chocolate", price: 1.20, quantity: 32, id: 3 }
    ]);

    return ( 
        <div className="home">
            <h2>Homepage</h2>
            <div>
                <ProductList products={products} title={"All products"} />
                {/* <ProductList products={products.filter((product) => product.title === "carrot")} title={"Carrots"} /> */}
            </div>
        </div>
     );
}
 
export default Home;