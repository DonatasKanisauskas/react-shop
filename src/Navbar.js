const Navbar = () => {
    return ( 
        <nav className="navbar">
            <p className="title">React Shop</p>
            <a href="/">Home</a>
            <a href="/cart">Cart</a>
        </nav>
     );
}
 
export default Navbar;