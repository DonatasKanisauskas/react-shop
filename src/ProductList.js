const ProductList = ({ products, title }) => {


    return (
        <div className="product-category">
            <h3 className="category-title">{title}</h3>
            <div className="product-list">
                {products.map((product) => (
                    <div className="product-card" key={product.id}>
                        <h4 className="product-title">{product.title}</h4>
                        <p className="product-content">$ {(product.price).toFixed(2)}</p>
                    </div>
                ))}
            </div>
        </div>
    );
}

export default ProductList;